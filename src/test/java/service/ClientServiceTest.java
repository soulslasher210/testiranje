/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.Client;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Krui
 */
public class ClientServiceTest {

    public Client a;
    
    public ClientServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    
    
   
    
     // postoje username i password, test bi trebao da se izvrsi uspesno
     
    @Test
    public void testLogin() {
        System.out.println("login");
        String username = "aleks100";
        String password = "aleks100";
        ClientService instance = new ClientService();
        Client expResult = a;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    // ne postoji username i password zato iskace null vrednost u test report
    @Test
    public void testLogin2() {
        System.out.println("login");
        String username = "nekiuser20";
        String password = "nesto";
        ClientService instance = new ClientService();
        Client expResult = null;
        Client result = instance.login(username, password);
        assertEquals(expResult, result);
    }
    

    
    
    // testRegister treba da prodje posto je unikatan i sve je popunjeno
     
    @Test
    public void testRegister() {
        System.out.println("register");
        String name = "Foster";
        String username = "Foster81";
        String password = "fost";
        ClientService instance = new ClientService();
        boolean expResult = true;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    
    
    //testRegister2 treba da padne posto vec ima isti username
    //Bug u kodu kod for petlje
    @Test
    public void testRegister2() {
        System.out.println("register");
        String name = "Aleksandar";
        String username = "aleks100";
        String password = "aleks100";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    // nismo uneli ime i trebalo bi da prodje
    @Test
    public void testRegister3() {
        System.out.println("register");
        String name = "";
        String username = "aleks100";
        String password = "aleks100";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    // nismo uneli username i trebalo bi da prodje
    @Test
    public void testRegister4() {
        System.out.println("register");
        String name = "Foster";
        String username = "";
        String password = "fost";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    // nismo uneli password i trebalo bi da prodje
    @Test
    public void testRegister5() {
        System.out.println("register");
        String name = "Foster";
        String username = "Foster81";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }
    // nismo uneli ni jedno polje i trebalo bi da prodje
    @Test
    public void testRegister6() {
        System.out.println("register");
        String name = "";
        String username = "";
        String password = "";
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.register(name, username, password);
        assertEquals(expResult, result);
    }

    //Kraj testiranja Register 
    
    
    //Pocetak testiranja DeleteUser
    
    // DeleteUser treba da prodje ali ce pasti zbog koda uvek vraca false iako je true
    @Test
    public void testDeleteUser() {
        System.out.println("deleteUser");
        Client c = new Client(1,"Aleksandar","aleks100","aleks100");
        ClientService instance = new ClientService();
        boolean expResult = true;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }
    
    //testDeleteUser2 ne postoji user sa ovim id
    @Test
    public void testDeleteUser2() {
        System.out.println("deleteUser");
        Client c = new Client(1,"a","nekiUser1","user1");
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }
    
    //testDeleteUser3 je pretraga po id posto u tabelama ne postoji korisnik sa id null bice false
    @Test
    public void testDeleteUser3() {
        System.out.println("deleteUser");
        Client c = new Client(null,"Aleksandar","aleks100","aleks100");;
        ClientService instance = new ClientService();
        boolean expResult = false;
        boolean result = instance.deleteUser(c);
        assertEquals(expResult, result);
    }
  

    
    
    
    
     
    
    
    //UpdateInfo ce da prodje posto su informacije tacno unete ali postoji error u kodu za proveru oldPassword
    //testUpdateInfo ima bug koji prilikom updateovanja korisnika, uvek će se updatovati na isto, prvobitno ime
    //bug
    @Test
    public void testUpdateInfo() {
        System.out.println("updateInfo");
        Client c = new Client(1,"Aleksandar","aleks100","aleks100");
        String name = "Marko";
        String oldPassword = "aleks100";
        String password = "aleks9";
        ClientService instance = new ClientService();
        instance.updateInfo(c, name, oldPassword, password);
    }
  
    
    //Kraj testiranja update
    
}
