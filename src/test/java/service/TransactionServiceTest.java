/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import model.Client;
import model.DeliveryService;
import model.ShopItem;
import model.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Krui
 */
public class TransactionServiceTest {
    
    public TransactionServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    //Pocetak testiranja
    
    //kod se nece testirati zbog TransactionService instance = null
    //Ako se napravi konstruktor za to u TransactionService, onda se menja error da nije supportovan 
   //bug
    @Test
    public void testCompleteTransaction() {
        System.out.println("completeTransaction");
        Client c = new Client(null,"aleks100","aleks100","aleks100");
        DeliveryService d = new DeliveryService(null,"postexpress",5,10);
        ShopItem s = new ShopItem(null,"Tortilla",75,10);
        int amount = 20;
        float distance = 100;
        TransactionService instance = null;
        instance.completeTransaction(c, d, s, amount, distance);
    }

    //kod se nece testirati zbog TransactionService instance = null
   //bug 
    @Test
    public void testGetRecentTransactions() {
        System.out.println("getRecentTransactions");
        TransactionService instance = null;
        ArrayList<Transaction> expResult = null;
        ArrayList<Transaction> result = instance.getRecentTransactions();
        assertEquals(expResult, result);
    }
    
}
