/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;
import model.DeliveryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Krui
 */
public class DeliveryServiceServiceTest {
    
    public DeliveryServiceServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    
  
    
    

    
     // nisu uneti podaci
     
    @Test
    public void testRegister() {
        System.out.println("register");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    // nije uneto ime
    @Test
    public void testRegister2() {
        System.out.println("register");
        String name = "";
        float pricePerKilometer = 5;
        float startingPrice = 100;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    // nije uneta cena po kilometru
    @Test
    public void testRegister3() {
        System.out.println("register");
        String name = "Taksi";
        float pricePerKilometer = 0.0F;
        float startingPrice = 100;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    //  nije uneta pocetna cena
    @Test
    public void testRegister4() {
        System.out.println("register");
        String name = "Taksi";
        float pricePerKilometer = 5;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    // nije uneto ime i pocetna cena 
    @Test
    public void testRegister5() {
        System.out.println("register");
        String name = "";
        float pricePerKilometer = 5;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    // nije uneto ime i cena po kilometru
    @Test
    public void testRegister6() {
        System.out.println("register");
        String name = "";
        float pricePerKilometer = 0.0F;
        float startingPrice = 100;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    // nije uneto cena po kilometru i pocetna cena
    @Test
    public void testRegister7() {
        System.out.println("register");
        String name = "Taksi";
        float pricePerKilometer = 0.0F;
        float startingPrice = 0.0F;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    //testRegister8 ce proci posto je sve uneto
    @Test
    public void testRegister8() {
        System.out.println("register");
        String name = "Taksi";
        float pricePerKilometer = 50;
        float startingPrice = 100;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.register(name, pricePerKilometer, startingPrice);
        assertEquals(expResult, result);
    }
    
    //Kraj testiranaj Register
    
    
    //Pocetak testiranja DeleteDeliveryService

    
    
    
    //testDeleteDeliveryService treba da vrati false, ali u kodu ne moze da vraca false nego samo true 
    // ne postoji ovaj user
    //bug
    @Test
    public void testDeleteDeliveryService() {
        System.out.println("deleteDeliveryService");
        DeliveryService d = new DeliveryService(3,"taksi",5,10); 
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.deleteDeliveryService(d);
        assertEquals(expResult, result);
    }
    
    //Kraj testiranja DeleteDeliveryService

    
    //Pocetak testiranja UpdateInfo
    
    //testUpdateInfo ce proci
    @Test
    public void testUpdateInfo() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(1,"postexpress",5,10);
        String name = "Amazon";
        float startingPrice = 10;
        float pricePerKilometer = 50;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = true;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    // testUpdateInfo2 id je null
    @Test
    public void testUpdateInfo2() {
        System.out.println("updateInfo");
        DeliveryService d = new DeliveryService(null,"postexpress",5,10);
        String name = "Amazon";
        float startingPrice = 10;
        float pricePerKilometer = 50;
        DeliveryServiceService instance = new DeliveryServiceService();
        boolean expResult = false;
        boolean result = instance.updateInfo(d, name, startingPrice, pricePerKilometer);
        assertEquals(expResult, result);
    }
    
    //Kraj testiranja UpdateInfo
    
}
