/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.ShopItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Krui
 */
public class ShopItemServiceTest {
    
    public ShopItemServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
   
    // ne mogu da se testiraju posto PromotionService ne postoji u bazi podataka

    
  

    //testPostItem ce proci posto je sve uneto
    @Test
    public void testPostItem() {
        System.out.println("postItem");
        String name = "Chips";
        float price = 53;
        int amount = 5;
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // unete su negativne vrednosti
    @Test
    public void testPostItem2() {
        System.out.println("postItem");
        String name = "Tortilla";
        float price = -21;
        int amount = -12;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // nije uneto name i  negative amount
    @Test
    public void testPostItem3() {
        System.out.println("postItem");
        String name = "";
        float price = 100;
        int amount = -1;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // ime je  prazan string i cena je negativna
    @Test
    public void testPostItem4() {
        System.out.println("postItem");
        String name = "";
        float price = -1;
        int amount = 5;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // unet je negativan broj na kolicini
    @Test
    public void testPostItem5() {
        System.out.println("postItem");
        String name = "Tortilla";
        float price = 190;
        int amount = -1;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // unet je negativan broj na ceni
    @Test
    public void testPostItem6() {
        System.out.println("postItem");
        String name = "Tomato";
        float price = -1;
        int amount = 6;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    // ime je prazan string
    @Test
    public void testPostItem7() {
        System.out.println("postItem");
        String name = "";
        float price = 100;
        int amount = 7;
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.postItem(name, price, amount);
        assertEquals(expResult, result);
    }
    
   
    
    
    

    // id je null 
    @Test
    public void testRemoveItem() {
        System.out.println("removeItem");
        ShopItem s = new ShopItem(null,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
    }
    
    // id postoji
    @Test
    public void testRemoveItem2 () {
        System.out.println("removeItem");
        ShopItem s = new ShopItem(1,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.removeItem(s);
        assertEquals(expResult, result);
    }

  
    
    
 
    
    
    
    
    //testBuy ima bug prilikom racunanja u ShopItemService gde se oduzima pocetni amount sa postavljenim amountom
    //tek posle oduzimanja proverava taj postavljeni amount sa oduzetim amountom i onda ispisuje gresku
    //bug
    @Test
    public void testBuy() {
        System.out.println("buy");
        ShopItem s = new ShopItem(1,"Tomato",75,10);
        int amount = 9;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
    }
    
    //testBuy2 ce ispisati da je broj negativan ali ce se takodje updatovati one iako je negativan rezultat 
    //bug
    @Test
    public void testBuy2() {
        System.out.println("buy");
        ShopItem s = new ShopItem(1,"Tortilla",75,10);
        int amount = -1;
        ShopItemService instance = new ShopItemService();
        instance.buy(s, amount);
    }
    
    //Kraj testiranja Buy
    
    
    //Pocetak StockUp
    
    

    //testStockUp ce proci, pisace da je negativan broj ali ce se svakako updatovati posto ce samo ici return if jer nema false i true
    
    @Test
    public void testStockUp() {
        System.out.println("stockUp");
        ShopItem s = new ShopItem(1,"Tortilla",75,10);
        int amount = -500;
        ShopItemService instance = new ShopItemService();
        instance.stockUp(s, amount);
    }
    
    //Kraj testiranja StockUp
    
    
    
    //Pocetak testiranja CheckIfPopular
    
    
    //Ovaj test ce uvek biti true u kodu, ne moze da bude false osim ako id ne postoji
    //Bug
    @Test
    public void testCheckIfPopular() {
        System.out.println("checkIfPopular");
        ShopItem s = new ShopItem(1,"Tortillavv",75,10);
        ShopItemService instance = new ShopItemService();
        boolean expResult = true;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    // CheckIfPopular moze da bude false samo ako je amountbought u kodu 0
    @Test
    public void testCheckIfPopular2() {
        System.out.println("checkIfPopular");
        ShopItem s = new ShopItem(5,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        boolean expResult = false;
        boolean result = instance.checkIfPopular(s);
        assertEquals(expResult, result);
    }
    
    //Kraj testiranja CheckIfPopular
    
    
    //Pocetak testiranja GetTrendingIndex 
    
   

    
   
     // testGetTrendingIndex ce proci, zato sto id postoji i onda se index povecava za taj broj 
    //i posle se to vreme racuna i dobije se broj 
    //bug
    @Test
    public void testGetTrendingIndex() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(1,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        float expResult = 435;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.1);
    }
    // id se ne podudara i indeks na kraju je 0
    @Test
    public void testGetTrendingIndex2() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(5,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        float expResult = 0;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.1);
    }
    
    // id ne postoji i izbacuje IllegalArgumentException
    @Test
    public void testGetTrendingIndex3() {
        System.out.println("getTrendingIndex");
        ShopItem s = new ShopItem(null,"Tortilla",75,10);
        ShopItemService instance = new ShopItemService();
        float expResult = 0;
        float result = instance.getTrendingIndex(s);
        assertEquals(expResult, result, 0.1);
    }
    
    //Kraj testiranja GetTrendingIndex
    
}
